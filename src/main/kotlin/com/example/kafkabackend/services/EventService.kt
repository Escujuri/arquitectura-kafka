package com.example.kafkabackend.services

import com.example.kafkabackend.domain.entities.BankAccount
import com.example.kafkabackend.domain.entities.Transaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service

@Service
class EventService {

    @Autowired
   lateinit var transactionKafkaTemplate: KafkaTemplate<String, Transaction>


    fun sendTransactionEvent(transaction: Transaction) {
        transactionKafkaTemplate.send("transaction_topic", transaction.transactionId.toString(), transaction)
    }
}
