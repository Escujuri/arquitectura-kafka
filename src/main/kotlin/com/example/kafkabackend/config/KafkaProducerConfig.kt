package com.example.kafkabackend.config


import com.example.kafkabackend.domain.entities.Transaction
import io.confluent.kafka.serializers.KafkaAvroSerializer
import org.apache.kafka.clients.CommonClientConfigs
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.config.SaslConfigs
import org.apache.kafka.common.config.SslConfigs
import org.apache.kafka.common.config.TopicConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.config.TopicBuilder
import org.springframework.kafka.core.*


@Configuration
class KafkaProducerConfig {

    // Configuración de la URL de los servidores de Kafka
    private val KAFKA_SERVERS = "b-2-public.prodarkakfag6.589us1.c2.kafka.us-east-2.amazonaws.com:9198,b-1-public.prodarkakfag6.589us1.c2.kafka.us-east-2.amazonaws.com:9198,b-3-public.prodarkakfag6.589us1.c2.kafka.us-east-2.amazonaws.com:9198"

    // Configuración de los esquemas Avro
    private val SCHEMA_REGISTRY_URL = "prodarclusterg6.1tj87u.c2.kafka.us-east-2.amazonaws.com:8081"

    private val transactionTopic = "transaction_topic"

    @Value("\${AWS_ACCESS_KEY_ID}")
    private lateinit var accessKeyId: String

    @Value("\${AWS_SECRET_ACCESS_KEY}")
    private lateinit var secretAccessKey: String

    @Bean
    fun kafkaAdmin(): KafkaAdmin {
        val configs = mutableMapOf<String, Any>()

        configs[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = KAFKA_SERVERS
        configs[CommonClientConfigs.SECURITY_PROTOCOL_CONFIG] = "SASL_SSL"
        configs[SaslConfigs.SASL_MECHANISM] = "AWS_MSK_IAM"
        configs[SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG] = "https"
        configs[SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG] = "JKS"

        // Configuración de autenticación IAM
        configs["sasl.jaas.config"] = "software.amazon.msk.auth.iam.IAMLoginModule required" +
                " aws.region=us-east-2" +
                " aws.accessKeyId=$accessKeyId" +
                " aws.secretKey=$secretAccessKey;"

        return KafkaAdmin(configs)
    }

    @Bean
    fun kafkaTemplate(): KafkaTemplate<String, Transaction> {
        return KafkaTemplate(producerFactory())
    }

    @Bean
    fun producerFactory(): ProducerFactory<String, Transaction> {
        val configs = mutableMapOf<String, Any>()

        configs[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = KAFKA_SERVERS
        configs[CommonClientConfigs.SECURITY_PROTOCOL_CONFIG] = "SASL_SSL"
        configs[SaslConfigs.SASL_MECHANISM] = "AWS_MSK_IAM"
        configs[SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG] = "https"
        configs[SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG] = "JKS"
        configs[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        configs[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = KafkaAvroSerializer::class.java
        configs["schema.registry.url"] = SCHEMA_REGISTRY_URL

        configs["sasl.jaas.config"] = "software.amazon.msk.auth.iam.IAMLoginModule required" +
                " aws.region=us-east-2" +
                " aws.accessKeyId=$accessKeyId" +
                " aws.secretKey=$secretAccessKey;"
        return DefaultKafkaProducerFactory(configs)
    }

    @Bean
    fun transactionTopic(): NewTopic {
        return TopicBuilder.name(transactionTopic)
            .partitions(1)
            .replicas(1)
            .config(TopicConfig.CLEANUP_POLICY_CONFIG, TopicConfig.CLEANUP_POLICY_COMPACT)
            .config(TopicConfig.RETENTION_MS_CONFIG, "604800000")
            .build()
    }
}
